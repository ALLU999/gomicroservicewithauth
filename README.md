# gomicroservicewithauth
This project shows how to add authorization layer to go micriservice api's.

there are two sample api's 
 => getPetDetails
 => getPetsSearch

user should pass access_token in the authorization header, the token is verified Against the Kecloak Server and checks the scopes of the user.


#project initialization
go mod init
go mod tidy

# run the project
go run main.go

