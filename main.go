package main

import (
	"encoding/json"
	"net/http"

	"Sample/DemoService/middleware"

	"github.com/julienschmidt/httprouter"
)

type PetDetails struct {
	Breed  string  `json:"breed"`
	Type   string  `json:"type"`
	Age    float32 `json:"age"`
	Gender string  `json:"gender"`
}

var pets map[string]PetDetails = map[string]PetDetails{
	"pinku": {
		Breed:  "pariah",
		Type:   "Dog",
		Age:    0.8,
		Gender: "Male",
	},
	"Blacky": {
		Breed:  "Afador",
		Type:   "Dog",
		Age:    1,
		Gender: "Female",
	},
	"Billi": {
		Breed:  "persian",
		Type:   "Cat",
		Age:    1,
		Gender: "Male",
	},
}

var petTypes map[string][]string = map[string][]string{
	"dog": {
		"Golden Retreiver",
		"Afador",
		"pariah",
		"Labrador",
		"German Shepherd",
	},
	"cat": {
		"Bombay Cat",
		"Himalayan Cat",
	},
}

func getPetDetails(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	name := r.URL.Query().Get("pet_name")
	petDetails := pets[name]
	data, _ := json.Marshal(petDetails)
	w.Write([]byte(data))
}

//returns an array
func getPetsSearch(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	typ := r.URL.Query().Get("pet_type")
	petBreeds := petTypes[typ]
	data, _ := json.Marshal(petBreeds)
	w.Write([]byte(data))
}

func main() {

	router := httprouter.New()
	router.GET("/demoservice/getPetDetails", middleware.IsAuthorizedJWT(getPetDetails, "pet-details"))
	router.GET("/demoservice/getPetsSearch", middleware.IsAuthorizedJWT(getPetsSearch, "pets-search"))
	http.ListenAndServe(":3333", router)
}
